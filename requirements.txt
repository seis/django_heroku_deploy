dj-database-url==0.4.1
Django==1.9.7
flake8==2.5.4
gunicorn==19.6.0
whitenoise==3.2
psycopg2==2.5.4
