import dj_database_url

DATABASES = {
    'default' : {
        dj_database_url.config(default=DATABASE_URL)
    }
}

DEBUG = False

SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
